package com.abcd.catalogue.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abcd.catalogue.entities.Stock;
import com.abcd.catalogue.repositories.StockRepository;

@Service
public class StockService {

	@Autowired
	private StockRepository stockRepository;

	public List<Stock> findById(Long stockId) {
		return stockRepository.findById(stockId);
	}

	public void save(Stock stock) {
		stockRepository.save(stock);
	}


	public List<Stock> getAllStocks(){
		Iterable<Stock> stocks = stockRepository.findAll();
		List<Stock> stockList = new ArrayList<Stock>();
		for (Stock stock : stocks) {
			stockList.add(stock);
		}
		return stockList;
	}
}
