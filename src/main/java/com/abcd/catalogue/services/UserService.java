package com.abcd.catalogue.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abcd.catalogue.entities.User;
import com.abcd.catalogue.repositories.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public List<User> findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	public void save(User user) {
		userRepository.save(user);
	}


	public List<User> getAllUsers(){
		Iterable<User> users = userRepository.findAll();
		List<User> userList = new ArrayList<User>();
		for (User user : users) {
			userList.add(user);
		}
		return userList;
	}
}
