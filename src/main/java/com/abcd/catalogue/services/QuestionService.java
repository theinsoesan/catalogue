package com.abcd.catalogue.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abcd.catalogue.entities.QuestionMessage;
import com.abcd.catalogue.repositories.QuestionRepository;

@Service
public class QuestionService {
	@Autowired
	private QuestionRepository questionRepository;

	public List<QuestionMessage> findByUsername(Long messageId) {
		return questionRepository.findByMessageId(messageId);
	}

	public void save(QuestionMessage message) {
		questionRepository.save(message);
	}


	public List<QuestionMessage> getAllQuestions(){
		Iterable<QuestionMessage> questionMessages = questionRepository.findAll();
		List<QuestionMessage> messageList = new ArrayList<QuestionMessage>();

		for(QuestionMessage questionMessage : questionMessages){
			messageList.add(questionMessage);
		}
		return messageList;
	}

}
