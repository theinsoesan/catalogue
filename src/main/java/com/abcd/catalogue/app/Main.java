package com.abcd.catalogue.app;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abcd.catalogue.entities.Stock;
import com.abcd.catalogue.entities.User;
import com.abcd.catalogue.services.StockService;
import com.abcd.catalogue.services.UserService;

public class Main {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");

		UserService userService = context.getBean(UserService.class);

		User user = new User();
		user.setPassword("test");
		user.setUsername("soe san");
		userService.save(user);

		List<User> users = userService.findByUsername("soe san");
		for (User searchedUser : users){
			System.out.println("password: " + searchedUser.getPassword());
		}

		StockService stockService = context.getBean(StockService.class);
		Stock stock = new Stock();
		stock.setDescription("Brand New");
		stock.setMyanmar_Description("Myanmar description");
		stock.setCategory_ID("DELL_1001");
		stock.setReport_Group_ID("1001");
		stock.setAccount_Set_ID("1001");
		stock.setStocking_UM("STOCK_UM");
		stock.setPricing_UM("Pricing_UM");
		stock.setDefault_UM("Default_UM");
		stock.setStatus((short) 3);
		stock.setValuation_Method((char) 1);
		stock.setRe_Order_Level(10001L);
		stock.setMaximum(100000001L);
		stock.setMinimum(10000L);
		stock.seteOQ(null);
		stock.setStocking_Price(100000.00);
		stock.setCountry_ID("MMY_18800");
		stock.setBrand_ID("DELL_H501");
		stock.setVolume("volume");
		stock.seteMDesc("Emdesc");
		stockService.save(stock);

		List<Stock> stockList = stockService.findById(1L);
		for (Stock searchedStock : stockList){
			System.out.println("Brand_ID is : " + searchedStock.getBrand_ID());
			System.out.println("Category_ID : "+ searchedStock.getCategory_ID());

		}


	}


}
