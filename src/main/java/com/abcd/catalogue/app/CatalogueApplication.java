package com.abcd.catalogue.app;


import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.abcd.catalogue.restservice.impl.UserRestServiceImpl;


public class CatalogueApplication extends Application {
	private final Set<Class<?>> classes = new HashSet<Class<?>>();

	public CatalogueApplication() {
		classes.add(UserRestServiceImpl.class);
	}

	@Override
	public Set<Class<?>> getClasses() {
		return classes;
	}
}