package com.abcd.catalogue.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.abcd.catalogue.entities.Stock;

public interface StockRepository extends CrudRepository<Stock, Long>{
	List<Stock> findById(Long id);

}
