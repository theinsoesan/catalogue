package com.abcd.catalogue.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.abcd.catalogue.entities.User;

public interface UserRepository extends CrudRepository<User, Long>  {
	List<User> findByUsername(String username);
}