package com.abcd.catalogue.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.abcd.catalogue.entities.QuestionMessage;

public interface QuestionRepository extends CrudRepository<QuestionMessage, Long>{
	List<QuestionMessage> findByMessageId(Long messageId);

}
