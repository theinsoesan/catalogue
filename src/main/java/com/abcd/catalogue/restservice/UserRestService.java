package com.abcd.catalogue.restservice;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.abcd.catalogue.entities.User;

@Path("services")
public interface UserRestService {

	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/users")
	public List<User> getAllUsers();



}
