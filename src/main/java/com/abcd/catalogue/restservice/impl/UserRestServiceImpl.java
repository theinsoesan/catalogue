package com.abcd.catalogue.restservice.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.abcd.catalogue.entities.User;
import com.abcd.catalogue.restservice.UserRestService;
import com.abcd.catalogue.services.UserService;

public class UserRestServiceImpl implements UserRestService{

	@Autowired
	private UserService userService;

	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}




}
