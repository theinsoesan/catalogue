package com.abcd.catalogue.restservice.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.abcd.catalogue.entities.Stock;
import com.abcd.catalogue.restservice.StockRestService;
import com.abcd.catalogue.services.StockService;

public class StockRestServiceImpl implements StockRestService{

	@Autowired
	private StockService stockService;

	public List<Stock> getAllStocks() {
		// TODO Auto-generated method stub
		return stockService.getAllStocks();
	}

}
