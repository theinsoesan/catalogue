package com.abcd.catalogue.restservice.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.abcd.catalogue.entities.QuestionMessage;
import com.abcd.catalogue.restservice.QuestionRestService;
import com.abcd.catalogue.services.QuestionService;

public class QuestionRestServiceImpl implements QuestionRestService{

	@Autowired
	private QuestionService questionService;
	public List<QuestionMessage> getAllQuestions() {
		// TODO Auto-generated method stub
		return questionService.getAllQuestions();
	}

}
