package com.abcd.catalogue.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class QuestionMessage {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long messageId;

	@ManyToOne
	private User user;

	@ManyToOne
	private Stock product;
	private String questionMessage;

	@Temporal(TemporalType.TIMESTAMP)
	private Date postedDate;
	private Short resovedFlag;
	public Long getMessageId() {
		return messageId;
	}
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Stock getProduct() {
		return product;
	}
	public void setProduct(Stock product) {
		this.product = product;
	}
	public String getQuestionMessage() {
		return questionMessage;
	}
	public void setQuestionMessage(String questionMessage) {
		this.questionMessage = questionMessage;
	}
	public Date getPostedDate() {
		return postedDate;
	}
	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}
	public Short getResovedFlag() {
		return resovedFlag;
	}
	public void setResovedFlag(Short resovedFlag) {
		this.resovedFlag = resovedFlag;
	}

}
