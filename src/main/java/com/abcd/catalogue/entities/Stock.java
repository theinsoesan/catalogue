package com.abcd.catalogue.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Stock{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String description;
	private String myanmar_Description;
	private String category_ID;
	private String report_Group_ID;
	private String account_Set_ID;
	private String stocking_UM;
	private String pricing_UM;
	private String default_UM;
	private Short status;
	private char Valuation_Method;
	private Long re_Order_Level;
	private Long maximum;
	private Long minimum;
	private Long eOQ;
	private Double stocking_Price;
	private String country_ID;
	private String brand_ID;
	private String volume;
	private String eMDesc;
	public Long getId() {
		return id;
	}
	public void setStock_ID(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMyanmar_Description() {
		return myanmar_Description;
	}
	public void setMyanmar_Description(String myanmar_Description) {
		this.myanmar_Description = myanmar_Description;
	}
	public String getCategory_ID() {
		return category_ID;
	}
	public void setCategory_ID(String category_ID) {
		this.category_ID = category_ID;
	}
	public String getReport_Group_ID() {
		return report_Group_ID;
	}
	public void setReport_Group_ID(String report_Group_ID) {
		this.report_Group_ID = report_Group_ID;
	}
	public String getAccount_Set_ID() {
		return account_Set_ID;
	}
	public void setAccount_Set_ID(String account_Set_ID) {
		this.account_Set_ID = account_Set_ID;
	}
	public String getStocking_UM() {
		return stocking_UM;
	}
	public void setStocking_UM(String stocking_UM) {
		this.stocking_UM = stocking_UM;
	}
	public String getPricing_UM() {
		return pricing_UM;
	}
	public void setPricing_UM(String pricing_UM) {
		this.pricing_UM = pricing_UM;
	}
	public String getDefault_UM() {
		return default_UM;
	}
	public void setDefault_UM(String default_UM) {
		this.default_UM = default_UM;
	}
	public Short getStatus() {
		return status;
	}
	public void setStatus(Short status) {
		this.status = status;
	}
	public char getValuation_Method() {
		return Valuation_Method;
	}
	public void setValuation_Method(char valuation_Method) {
		Valuation_Method = valuation_Method;
	}
	public Long getRe_Order_Level() {
		return re_Order_Level;
	}
	public void setRe_Order_Level(Long re_Order_Level) {
		this.re_Order_Level = re_Order_Level;
	}
	public Long getMaximum() {
		return maximum;
	}
	public void setMaximum(Long maximum) {
		this.maximum = maximum;
	}
	public Long getMinimum() {
		return minimum;
	}
	public void setMinimum(Long minimum) {
		this.minimum = minimum;
	}
	public Long geteOQ() {
		return eOQ;
	}
	public void seteOQ(Long eOQ) {
		this.eOQ = eOQ;
	}
	public Double getStocking_Price() {
		return stocking_Price;
	}
	public void setStocking_Price(Double stocking_Price) {
		this.stocking_Price = stocking_Price;
	}
	public String getCountry_ID() {
		return country_ID;
	}
	public void setCountry_ID(String country_ID) {
		this.country_ID = country_ID;
	}
	public String getBrand_ID() {
		return brand_ID;
	}
	public void setBrand_ID(String brand_ID) {
		this.brand_ID = brand_ID;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String geteMDesc() {
		return eMDesc;
	}
	public void seteMDesc(String eMDesc) {
		this.eMDesc = eMDesc;
	}


}
